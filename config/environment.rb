require 'crack'
require 'json'
require 'gmail_xoauth'
require 'mail'
require 'pp'
require 'two-legged-oauth'
require 'google_drive'
require 'oauth'
require 'rexml/document'
require 'httparty'
require 'gprov/auth'
require 'gprov/connection'
require 'gprov/provision'
require 'gprov'
require 'mongoid'

$LOAD_PATH.unshift("#{File.dirname(__FILE__)}")

# load some configuration values
raw_config = File.read( "config/config.yml")
APP_CONFIG = YAML.load(raw_config)
ENV['MONGOID_ENV'] = 'production'
Mongoid.load!("config/mongoid.yml")


class AccountUidLut
  include Mongoid::Document
  include Mongoid::Timestamps

  field :account, type: String
  field :uid, type: Integer, default: 0
end

# monkey-patch the annoying OAuth Logger
module OAuth
  class Logger
    def self.debug(message)
      nil
    end
  end
end

# stolen from https://github.com/dcparker/ruby-gmail/issues/11
# monkey-patch Net::IMAP for X-GM-MSGID
module Net
  class IMAP
    class ResponseParser 
      def msg_att
        match(T_LPAR)
        attr = {}
        while true
          token = lookahead
          case token.symbol
          when T_RPAR
            shift_token
            break
          when T_SPACE
            shift_token
            token = lookahead
          end
          case token.value
          when /\A(?:ENVELOPE)\z/ni
            name, val = envelope_data
          when /\A(?:FLAGS)\z/ni
            name, val = flags_data
          when /\A(?:INTERNALDATE)\z/ni
            name, val = internaldate_data
          when /\A(?:RFC822(?:\.HEADER|\.TEXT)?)\z/ni
            name, val = rfc822_text
          when /\A(?:RFC822\.SIZE)\z/ni
            name, val = rfc822_size
          when /\A(?:BODY(?:STRUCTURE)?)\z/ni
            name, val = body_data
          when /\A(?:UID)\z/ni
            name, val = uid_data

          when /\A(?:X-GM-MSGID)\z/ni  # Added X-GM-MSGID extension
            name, val = uid_data
          when /\A(?:X-GM-THRID)\z/ni  # Added X-GM-THRID extension
            name, val = uid_data

          else
            parse_error("unknown attribute `%s'", token.value)
          end
          attr[name] = val
        end
        return attr
      end

    end
  end
end