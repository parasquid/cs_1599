This is an entry to challenge#1599 - centrally archive all google chat logs.

Some important notes about the technical decisions:

* The logs are saved to one document per conversation. This is intentional to
  aid in scalability. If a spreadsheet were chosen, the document will be limited
  to 400,000 cells. The same with putting all conversations of an account to the
  same document - it will be limited to 1billion characters.

* Thus, the best way is to just create an account that will be solely be used
  to upload the chatlogs.

* Documents were chosen instead of spreadsheets because they are more searchable
  (at least with the limited testing I did, I was not able to search through
  worksheet cells).

* Ideally the chatlogs _should_ be saved in a database for maximum searchability
  and maintenance, but the challenge requires for google docs integration so
  there you go.

* The script is dependent on a MongoDB database. I was debating on whether I
  should just use a flatfile csv or yaml file, but then the scalability caveat
  came into place. The database is only used to store the uid of the chatlog
  messages so that the script doesn't have to reparse the whole chatlogs from the
  beginning. This means the db won't grow so large (it will only grow if the
  number of accounts in the domain increase). You can use one of the free dbs
  from MongoLab if you want (they give away as much as 240MB per free db)

* Since we're using IMAP, each account needs to be configured to have IMAP
  capable (an admin can do this globally)

* Each account must also check the box "Show in IMAP" in their label settings
  (see http://cl.ly/0Y2V0s130c1p2S3O2B0W) I haven't been able to find out a
  way for an admin to set this up globally, but I guess there's a way to do that

* To get the list of users, the provisioning API must be enabled
  (http://cl.ly/1K3K3e132A1c03420i2S)

Installation:

This is a ruby program and thus requires to have ruby installed on your system.
I'm using the new hash syntax so you'll need ruby version 1.9.2 or above (I
developed this using 1.9.3).

I used Gemfiles so you'll need to install bundler and install the gems. I use
RVM so I don't need to go sudo on gem installation, but if you're using the ruby
from your package manager, you might need to do so.

  $ cd ~/scripts/cs_1599
  $ gem install bundler
  $ bundle install

The program is mostly a collection of Rakefiles so you'll need rake as well.

  $ gem install rake

You'll also need to install MongoDB if you're using your own server, or just use
one of the free dbs provided by MongoLab or MongoHQ.

You'll then need to configure the script. You'll need to have the following:

* consumer key and secret, which is the keys required for two-legged
  authentication (see this page for a guide
  http://support.google.com/a/bin/answer.py?hl=en&answer=162105) - it's easiest
  to just put "Allow access to all APIs" but if you want more fine grained
  control, just follow the guide on how to manage third party API keys

* an admin password and account - I wasn't able to get two-legged authentication
  work with the provisioning API so we'll just use clientLogin for now. I'd
  recommend making a separate account that is admin, with a really strong
  password and this account to be used only for managing chatlogs

* a document account - this account will be the default owner of the documents
  that the script will upload. You'll need to create this account in GApps as
  well

* the acl (access control list) - I've provided a few samples, but this is to
  easily control how the chatlogs are shared

I've provided a rake task to help install a cronjob (the configuration on how
often this cronjob runs is at config/schedule.rb)

  $ rake install_cronjob

However, if you're running the script for the first time, you might want to
call the rake task manually first especially if you have hundreds of users. The
script might take a number of hours to run.

  $ rake archive_chat_logs

Good luck!