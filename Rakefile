task :environment do  
  require File.expand_path(File.join(*%w[ config environment ]),File.dirname(__FILE__))
end

desc 'lists all of the users'
task :list_all_users => :environment do
  users = get_user_list(admin_account: APP_CONFIG[:admin_account], password: APP_CONFIG[:admin_password], domain: APP_CONFIG[:domain])
  print_table users[:user_table]
end

desc 'list chatlogs of all accounts'
task :list_chatlogs => :environment do
  users = get_user_list(admin_account: APP_CONFIG[:admin_account], password: APP_CONFIG[:admin_password], domain: APP_CONFIG[:domain])

  # instead of iterating through the whole userlist in a single loop, you might
  # want to slice the userlist and feed them into different threads. that way
  # each chat retrieval happens on a different connection.
  users[:accounts].each do |account|
    puts "processing #{account}"
    begin
      chatlog = get_chat_logs account
      pp chatlog
    rescue Exception => e
      puts e
    end
  end
end

desc 'install cronjob'
task :install_cronjob do
  `whenever --update-crontab`
end

desc 'centrally archive all google chat logs'
task :archive_chat_logs => :environment do
  oauth_consumer = OAuth::Consumer.new(APP_CONFIG[:consumer_key], APP_CONFIG[:consumer_secret])
  access_token = OAuth::TwoLeggedAccessToken.new(oauth_consumer, APP_CONFIG[:document_account])
  session = GoogleDrive.login_with_oauth(access_token)

  users = get_user_list(admin_account: APP_CONFIG[:admin_account], password: APP_CONFIG[:admin_password], domain: APP_CONFIG[:domain])

  # instead of iterating through the whole userlist in a single loop, you might
  # want to slice the userlist and feed them into different threads. that way
  # each chat retrieval happens on a different connection.
  users[:accounts].each do |account|
    puts "processing #{account}"
    begin
      chatlog = get_chat_logs account
      chatlog.each do |chat|
        file = session.upload_from_string stringify(chat),
          "#{account}-#{chat[:message_id]}", content_type: "text/plain"
        puts "uploaded #{file.title}"
        APP_CONFIG[:acl].each do |acl|
          puts acl
          begin
            file.acl.push acl
          rescue Exception => e
            # we don't want to stop in case of invalid scopes
            puts e
          end
        end
      end

    rescue Exception => e
      # some accounts cannot be accessed, log them but don't stop for them
      puts e
    end
  end
end

private

# Convenience function to turn a chat array into text
def stringify(chat)
  conversation = []
  chat[:conversation].each do |message|
    conversation << "#{message[:timestamp]} : #{message[:from]} => #{message[:body]}"
  end
  conversation.join("\n")
end

# Gets the chat logs for an account
# Returns an array of chatlogs
def get_chat_logs(account)
  imap = Net::IMAP.new('imap.gmail.com', 993, usessl=true, certs=nil, verify=false)
  imap.authenticate('XOAUTH', account, :two_legged=>true, consumer_key: APP_CONFIG[:consumer_key], consumer_secret: APP_CONFIG[:consumer_secret])

  imap.examine('[Gmail]/Chats')

  chatlog = []
  account = AccountUidLut.find_or_create_by(account: account)

  # we add 1 to uid because we've already saved this one. the imap search
  # command is inclusive so we want to start with the next uid. uids are not
  # guaranteed to be sequential though, but this technique still works because
  # all we want to do is to skip the first search result.
  imap.uid_search("#{account.uid + 1}:*").each do |message_id|
    imap_msg = imap.uid_fetch(message_id, ['RFC822', 'X-GM-MSGID'])
    msg = imap_msg.first.attr['RFC822']
    uid = imap_msg.first.attr['UID']
    mail = Mail.new(msg)

    if mail.multipart?
      mail.parts.each do |part|
        chat = build(part, mail.message_id)
        unless chat.nil? || uid == account.uid
          chatlog << chat

          # we start off with this uid the next time around
          account.uid = uid if uid > account.uid

          # we save here in case we get disconnected, feel free to move this
          # outside the loop if you need more performance vs. reliability (but
          # mongodb is fast and scalable enough to not worry about this)
          account.save
        end
      end
    else
      # we're not really interested in this; chat messages are always multipart
      puts mail.body.decoded
    end
  end
  chatlog
end

def build(part, message_id)
  begin
    chat = {}
    conversation = []
    doc = Crack::XML.parse(part.body.decoded).to_hash
    if((messages = doc['con:conversation']['cli:message']).kind_of? Array)
      # conversation here
      messages.each do |message|
        next unless message['cli:body']
        msg = decode message
        conversation << msg
      end
    else
      # single message
        msg = decode messages
        conversation << msg
    end
    chat[:message_id] = message_id
    chat[:conversation] = conversation
    chat
  rescue REXML::ParseException => e
    # swallow this, as we're not really interested in the other part
  end
end

# Convenience function to decode message elements into a hash
def decode(message)
  msg = {}
  msg[:timestamp] = Time.at(message['time']['ms'].to_i / 1000).to_s
  msg[:from] = message['from']
  msg[:body] = message['cli:body']
  msg
end

# stolen from http://rubydoc.info/github/wycats/thor/Thor/Shell/Basic:print_table
def print_table(table, options={})
  return if table.empty?

  formats, indent, colwidth = [], options[:indent].to_i, options[:colwidth]
  options[:truncate] = terminal_width if options[:truncate] == true

  formats << "%-#{colwidth + 2}s" if colwidth
  start = colwidth ? 1 : 0

  colcount = table.max{|a,b| a.size <=> b.size }.size

  start.upto(colcount - 2) do |i|
    maxima ||= table.map {|row| row[i] ? row[i].size : 0 }.max
    formats << "%-#{maxima + 2}s"
  end

  formats[0] = formats[0].insert(0, " " * indent)
  formats << "%s"

  table.each do |row|
    sentence = ""

    row.each_with_index do |column, i|
      sentence << formats[i] % column.to_s
    end

    sentence = truncate(sentence, options[:truncate]) if options[:truncate]
    puts sentence
  end
end

# partly stolen from https://github.com/adrienthebo/gtool/blob/master/lib/gtool/auth.rb
def get_user_list(options)
  admin_account = options[:admin_account]
  password = options[:password]
  domain = options[:domain]
  credentials = authenticate(admin_account: admin_account, password: password, service: 'apps')
  connection = GProv::Connection.new(domain, credentials[:token])
  users = GProv::Provision::User.all(connection)
  fields = GProv::Provision::User.attribute_names
  field_names = GProv::Provision::User.attribute_titles

  rows = users.map do |user|
    fields.map {|f| user.send f}
  end
  rows.unshift field_names
  {user_table: rows, accounts: users.map { |user| "#{user.user_name}@#{domain}" } }
end

# stolen from https://github.com/adrienthebo/gtool/blob/master/lib/gtool/auth.rb
def authenticate(options)
  token_duration = 60 * 60 * 24 * 14
  authject = GProv::Auth::ClientLogin.new(options[:admin_account], options[:password], options[:service])
  token = authject.token

  if token.nil?
    puts "Authentication failed!"
    false
  else
    puts "Authentication accepted, token valid till #{Time.now + token_duration}"
    credentials = {:token => token, :created => Time.now}
    puts YAML.dump(credentials)
    credentials
  end
end

